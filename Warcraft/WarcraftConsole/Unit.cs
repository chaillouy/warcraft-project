using System;
using WarcraftConsole;
namespace warcraft
{
    public partial class Unit
    {
        public string type;
        public string race;
        public string faction;
        public int hit_points;
        public int armor;


        public Unit(string type, string race, string faction, int hit_points, int armor)
        {
        this.type=type;
        this.race=race;
        this.faction=faction;
        this.hit_points=hit_points;
        this.armor=armor;
        }


        public string sayHelloHorde()
        {
            return ("POUR LA HORDE !!!!!!!");
        }
         public string sayHelloAlliance()
        {
            return ("POUR L'alliance !!!!!!!");
        }

        public string grunt()
        {
            return ("Greu");
        }
         public string talkHorde()
        {
            return ("Travail termine !");
        }
         public string talkAlliance()
        {
            return ("Encore du travail ?");
        }
         public string talkToPeon(Unit peon1,Unit Peon2)
        {
            return ("Lok'Tar Ogar, la victoire ou la mort !!");
        }
         public string talkToPeasant()
        {
            return ("bonjour comment aller vous ?");
        }
    }
}