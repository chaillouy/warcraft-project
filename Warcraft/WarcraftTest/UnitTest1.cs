using System;
using Xunit;
using WarcraftConsole;
using warcraft;
namespace WarcraftTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestUnitConstructor()
        {
        Unit peon = new Unit("peon","orc","Horde",30,0);

        Assert.Equal("peon",peon.type);
        Assert.Equal("orc",peon.race);
        Assert.Equal("Horde",peon.faction);
        Assert.Equal(30,peon.hit_points);
        Assert.Equal(0,peon.armor);


        Unit peasant = new Unit("peasant","human","alliance",30,0);

        Assert.Equal("peasant",peasant.type);
        Assert.Equal("human",peasant.race);
        Assert.Equal("alliance",peasant.faction);
        Assert.Equal(30,peasant.hit_points);
        Assert.Equal(0,peasant.armor);

        }
      
        [Fact]
        public void TestUnitSayHello()
        {
        Unit peon = new Unit("peon","orc","Horde",30,0);
        Assert.Equal("POUR LA HORDE !!!!!!!", peon.sayHelloHorde());
        
        Unit peasant = new Unit("peasant","human","alliance",30,0);
        Assert.Equal("POUR L'alliance !!!!!!!", peasant.sayHelloAlliance());

        }  

        [Fact]
        public void TestUnitGrunt()
        {
        Unit peon = new Unit("peon","orc","Horde",30,0);
        Assert.Equal("Greu", peon.grunt());

        
        Unit peasant = new Unit("peasant","human","alliance",30,0);
        Assert.Equal("Greu", peasant.grunt());

        }

        [Fact]
        public void TestUnitTalk()
        {
        Unit peon = new Unit("peon","orc","Horde",30,0);
        Assert.Equal("Travail termine !", peon.talkHorde());
        
        Unit peasant = new Unit("peasant","human","alliance",30,0);
        Assert.Equal("Encore du travail ?", peasant.talkAlliance());

        }
    

        [Fact]
        public void TestUnitTalkToPeon()
        {


        Boss Thrall = new Boss("Thrall");
        Boss Garrosh = new Boss("Garrosh");

        Assert.Equal("Lok'Tar Ogar, la victoire ou la mort !!",Thrall.talkToPeon(Thrall,Garrosh));
        
        }

        [Fact]
        public void TestUnitTalkToPeasant()
        {
       
        Boss varian = new Boss("varian");
        Boss velen = new Boss("velen");
        Assert.Equal("bonjour comment aller vous ?", varian.talkToPeasant(varian,velen));
        
        }
    }
}
